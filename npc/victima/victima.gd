extends Area2D
class_name Victima

var _rescued : bool = false
var _detectorEntered : int = 0

func _ready():
	$sfxGrito.stream = SingletonSfx.get_random_scream()

func do_scream():
	if !_rescued:
		match _detectorEntered:
			1: #anillo externo
				AudioServer.set_bus_effect_enabled(1, 0, false)
				print("Anillo externo")
			2: #anillo medio
				AudioServer.set_bus_effect_enabled(1, 1, false)
				AudioServer.set_bus_effect_enabled(1, 2, true)
				print("Anillo interno")
		$scream_timer.start()		
	
func do_found():
	if !_rescued:
		$AnimatedSprite.play("rescued")
		_rescued = true
			
func _on_sfxGrito_finished():
	print("# victim-finished-screaming")

func _on_PlayerDetector_body_shape_entered(_body_id, body, _body_shape, _area_shape):
	if body is Player:
		_detectorEntered += 1
		#printt(str(body_id),str(body),str(body_shape),str(area_shape),str(_detectorEntered))

func _on_AnimatedSprite_animation_finished():
	$AnimatedSprite.play("buried")
	queue_free()

func _on_scream_timer_timeout():
	$sfxGrito.play()
