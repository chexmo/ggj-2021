extends Node#singletonSfx
var _scream01 = load("res://Player/gritos - player.ogg")
var _scream02 = load("res://Player/gritos - player(2).ogg")
var _scream03 = load("res://Player/gritos - player(3).ogg")
var _scream04 = load("res://Player/gritos - player(4).ogg")

var _randomNum = RandomNumberGenerator.new()

func _ready():
	_randomNum.randomize()
	pass

func get_random_num(cant) -> int:
	var num = _randomNum.randi_range(1,cant)
	return num
	
func get_random_scream():
	var num = get_random_num(4)
	var scream	
	match num:
		1: scream = _scream01
		2: scream = _scream02
		3: scream = _scream03
		4: scream = _scream04
	print("# scream " + str(num) + " sended to player")
	return scream
