extends Area2D
#class_name Classname #, "res://path/to/icon.svg"

# ------------------------------------------- ENUMS and CONSTS
enum DIR_MODE {OMNI, EIGHTWAY, FOURWAY}

# ------------------------------------------- EXPORTABLE VARS
export var visual_delta : int = 20  # Pixeles que se puede mover el pad
export(DIR_MODE) var mode
export var icon_res : Texture = load("res://icon.png")
export var icon_scale : float = 1

# ------------------------------------------- SIGNAL VARS
signal swipe_started
signal swipe_ended(dir)

# ------------------------------------------- ATTRIBUTE VARS
var direction : Vector2 = Vector2.ZERO
var is_swiping : bool = false

# ------------------------------------------- ONREADY VARS

# ------------------------------------------- LIFECYCLE METHODS
#func _enter_tree(): 
func _ready(): 
	$Icon.texture = icon_res
	$Icon.scale *= icon_scale

func _process(_delta):
	if direction.length() > 1 : direction = direction.normalized()
	$Pad_fg.position = direction * visual_delta

#func _physics_process(delta):
#func _exit_tree():

# ------------------------------------------- OVERRIDABLE METHODS
func _unhandled_input(event: InputEvent) -> void:
	if is_swiping:
		if event is InputEventScreenDrag and is_swiping:
			calc_direction(position.direction_to(event.position))
		if event is InputEventScreenTouch and not event.is_pressed():
			printt("PAD: just released" ,"dir:",direction)
			emit_signal("swipe_ended",direction.normalized())
			is_swiping = false
			direction = Vector2.ZERO

# ------------------------------------------- CUSTOM METHODS
func calc_direction(swipe_direction : Vector2):
	var a : float = swipe_direction.angle() / PI
	if mode == DIR_MODE.OMNI:
		direction =  swipe_direction.normalized()
	elif mode == DIR_MODE.EIGHTWAY:
		if a >= -7/8.0 and a < -5/8.0:
			direction = Vector2(-1,-1).normalized()
		elif a >= -5/8.0 and a < -3/8.0:
			direction = Vector2(0,-1).normalized()
		elif a >= -3/8.0 and a < -1/8.0:
			direction = Vector2(1,-1).normalized()
		elif a >= -1/8.0 and a < 1/8.0:
			direction = Vector2(1,0).normalized()
		elif a >= 1/8.0 and a < 3/8.0:
			direction = Vector2(1,1).normalized()
		elif a >= 3/8.0 and a < 5/8.0:
			direction = Vector2(0,1).normalized()
		elif a >= 5/8.0 and a < 7/8.0:
			direction = Vector2(-1,1).normalized()
		elif a < -7/8.0 or a >= 7/8.0:
			direction = Vector2(-1,0).normalized()
	elif mode == DIR_MODE.FOURWAY:
		if a >= -6/8.0 and a < -2/8.0:
			direction = Vector2(0,-1).normalized()
		elif a >= -2/8.0 and a < 2/8.0:
			direction = Vector2(1,0).normalized()
		elif a >= 2/8.0 and a < 6/8.0:
			direction = Vector2(0,1).normalized()
		elif a < -6/8.0 or a >= 6/8.0:
			direction = Vector2(-1,0).normalized()

# ------------------------------------------- SIGNAL METHODS
func _on_Pad_input_event(_viewport: Node, event: InputEvent, _shape_idx: int) -> void:
	if event is InputEventScreenTouch:
		if event.is_pressed():
			emit_signal("swipe_started")
			print("PAD: just pressed")
			is_swiping = true
