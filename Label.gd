extends Label
#class_name Classname #, "res://path/to/icon.svg"

# ------------------------------------------- ENUMS and CONSTS
# ------------------------------------------- EXPORTABLE VARS
# ------------------------------------------- SIGNAL VARS
# ------------------------------------------- ATTRIBUTE VARS
# ------------------------------------------- ONREADY VARS
# ------------------------------------------- LIFECYCLE METHODS
#func _enter_tree(): 
#func _ready(): 
#func _process(delta): 
#func _physics_process(delta):
#func _exit_tree():

# ------------------------------------------- OVERRIDABLE METHODS
# ------------------------------------------- CUSTOM METHODS

func set_visible_():
	$Timer.start()
	visible = true

# ------------------------------------------- SIGNAL METHODS

func _on_Timer_timeout() -> void:
	visible = false
