extends Node2D

var cant_victimas = 0
var max_victimas = 0

func _ready():
	for staticAux in $Statics.get_children():
			staticAux.z_index = staticAux.position.y
	cant_victimas = $Victimas.get_child_count()
	max_victimas = $Victimas.get_child_count()
	$Node2D/HUD.set_vict(max_victimas - cant_victimas,max_victimas)


func _process(delta: float) -> void:
	$Node2D/HUD.set_energia($Player.energy,$Player.max_energy)

func _on_Player_victim_found() -> void:
	cant_victimas -= 1
	$Node2D/HUD.set_vict(max_victimas - cant_victimas,max_victimas)
	$Label.set_visible_()
	if cant_victimas ==  0:
		$Label.text="¡Victimas rescatadas!"
		$resetGame.start()
		
func _on_resetGame_timeout():
	get_tree().change_scene("res://MainScene.tscn")
